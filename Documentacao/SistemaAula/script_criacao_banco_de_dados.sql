-- MySQL dump 10.13  Distrib 5.6.25, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: progestnet
-- ------------------------------------------------------
-- Server version	5.6.25-0ubuntu1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alunos`
--

DROP TABLE IF EXISTS `alunos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alunos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `CPF` varchar(11) COLLATE utf8_bin NOT NULL,
  `RG` varchar(15) COLLATE utf8_bin NOT NULL,
  `nome` varchar(45) COLLATE utf8_bin NOT NULL,
  `sexo` varchar(1) COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `tel_residencial` varchar(18) COLLATE utf8_bin DEFAULT NULL,
  `tel_celular` varchar(18) COLLATE utf8_bin DEFAULT NULL,
  `end_logradouro` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `end_numero` varchar(6) COLLATE utf8_bin DEFAULT NULL,
  `end_complemento` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `end_bairro` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `end_cidade` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `end_uf` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `end_cep` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `data_nascimento` varchar(10) COLLATE utf8_bin NOT NULL,
  `escolaridade_id` int(11) DEFAULT NULL,
  `situacao_financeira` int(11) DEFAULT NULL,
  `ano_processo_seletivo` varchar(4) COLLATE utf8_bin NOT NULL,
  `semestre_processo_seletivo` varchar(2) COLLATE utf8_bin NOT NULL,
  `login` varchar(45) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `CPF_UNIQUE` (`CPF`),
  UNIQUE KEY `login_UNIQUE` (`login`),
  KEY `fk_alunos_2_idx` (`escolaridade_id`),
  CONSTRAINT `fk_alunos_1` FOREIGN KEY (`login`) REFERENCES `usuarios` (`login`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_alunos_2` FOREIGN KEY (`escolaridade_id`) REFERENCES `escolaridades` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `atividades`
--

DROP TABLE IF EXISTS `atividades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `atividades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `valor` double unsigned zerofill NOT NULL,
  `descricao` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `data_aplicacao` date DEFAULT NULL,
  `turma_disciplina_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Atividade_1_idx` (`turma_disciplina_id`),
  CONSTRAINT `fk_atividades_1` FOREIGN KEY (`turma_disciplina_id`) REFERENCES `turma_disciplina` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `aulas`
--

DROP TABLE IF EXISTS `aulas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aulas` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `data` date NOT NULL,
  `hora` time NOT NULL,
  `ementa` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `turma_disciplina_id` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`),
  KEY `fk_Aula_1_idx` (`turma_disciplina_id`),
  CONSTRAINT `fk_aulas_1` FOREIGN KEY (`turma_disciplina_id`) REFERENCES `turma_disciplina` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `candidatos`
--

DROP TABLE IF EXISTS `candidatos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `candidatos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cpf` varchar(11) COLLATE utf8_bin NOT NULL,
  `nome_completo` varchar(45) COLLATE utf8_bin NOT NULL,
  `data_nascimento` date NOT NULL,
  `tel_residencial` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `tel_celular` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `end_logradouro` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `end_numero` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `end_complemento` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `end_bairro` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `end_cidade` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `end_estado` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `end_cep` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `escolaridade_id` int(11) DEFAULT NULL,
  `data_cadastro` datetime NOT NULL,
  `login` varchar(45) COLLATE utf8_bin NOT NULL,
  `senha` varchar(45) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cpf_UNIQUE` (`cpf`),
  KEY `fk_candidato_1_idx` (`escolaridade_id`),
  CONSTRAINT `fk_candidato_1` FOREIGN KEY (`escolaridade_id`) REFERENCES `escolaridades` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `coordenadores`
--

DROP TABLE IF EXISTS `coordenadores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coordenadores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `voluntario_id` int(11) NOT NULL,
  `curso_id` int(11) NOT NULL,
  `data_inicio` date NOT NULL,
  `data_fim` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_coordenadores_1_idx` (`curso_id`),
  KEY `fk_coordenadores_2_idx` (`voluntario_id`),
  CONSTRAINT `fk_coordenadores_1` FOREIGN KEY (`curso_id`) REFERENCES `cursos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_coordenadores_2` FOREIGN KEY (`voluntario_id`) REFERENCES `voluntarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cursos`
--

DROP TABLE IF EXISTS `cursos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cursos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `disciplinas`
--

DROP TABLE IF EXISTS `disciplinas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `disciplinas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) COLLATE utf8_bin NOT NULL,
  `descricao` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `ementa` text COLLATE utf8_bin,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `escolaridades`
--

DROP TABLE IF EXISTS `escolaridades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `escolaridades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome_UNIQUE` (`nome`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `inscricoes`
--

DROP TABLE IF EXISTS `inscricoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inscricoes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ano` varchar(5) COLLATE utf8_bin NOT NULL,
  `semestre` varchar(2) COLLATE utf8_bin NOT NULL,
  `candidato_id` int(11) NOT NULL,
  `curso_id` int(11) NOT NULL,
  `nota` double unsigned DEFAULT NULL,
  `aprovado` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_inscricoes_1_idx` (`candidato_id`),
  KEY `fk_inscricoes_2_idx` (`curso_id`),
  CONSTRAINT `fk_inscricoes_1` FOREIGN KEY (`candidato_id`) REFERENCES `candidatos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_inscricoes_2` FOREIGN KEY (`curso_id`) REFERENCES `cursos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nota_atividade`
--

DROP TABLE IF EXISTS `nota_atividade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nota_atividade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nota` double unsigned zerofill NOT NULL,
  `aluno_id` int(11) NOT NULL,
  `atividade_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_nota_atividade_1_idx` (`aluno_id`),
  KEY `fk_nota_atividade_2_idx` (`atividade_id`),
  CONSTRAINT `fk_nota_atividade_1` FOREIGN KEY (`aluno_id`) REFERENCES `alunos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_nota_atividade_2` FOREIGN KEY (`atividade_id`) REFERENCES `atividades` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `presencas`
--

DROP TABLE IF EXISTS `presencas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `presencas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `presente` int(1) unsigned zerofill NOT NULL,
  `aluno_id` int(11) NOT NULL,
  `aula_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_presencas_1_idx` (`aluno_id`),
  KEY `fk_presencas_2_idx` (`aula_id`),
  CONSTRAINT `fk_presencas_1` FOREIGN KEY (`aluno_id`) REFERENCES `alunos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_presencas_2` FOREIGN KEY (`aula_id`) REFERENCES `aulas` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `registro_ponto`
--

DROP TABLE IF EXISTS `registro_ponto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registro_ponto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hora_inicio` datetime NOT NULL,
  `hora_fim` datetime DEFAULT NULL,
  `horas_trabalhadas` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `descricao_atividade` tinytext COLLATE utf8_bin,
  `voluntario_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_RegistroPonto_1_idx` (`voluntario_id`),
  CONSTRAINT `fk_registro_ponto_1` FOREIGN KEY (`voluntario_id`) REFERENCES `voluntarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `turma_disciplina`
--

DROP TABLE IF EXISTS `turma_disciplina`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `turma_disciplina` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `turma_id` int(11) NOT NULL,
  `disciplina_id` int(11) NOT NULL,
  `voluntario_id` int(11) DEFAULT NULL COMMENT 'Formador da disciplina',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ID_UNIQUE` (`id`),
  KEY `fk_TurmaDisciplina_1_idx` (`turma_id`),
  KEY `fk_TurmaDisciplina_2_idx` (`disciplina_id`),
  KEY `fk_turma_disciplina_1_idx` (`voluntario_id`),
  CONSTRAINT `fk_turma_disciplina_1` FOREIGN KEY (`disciplina_id`) REFERENCES `disciplinas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_turma_disciplina_2` FOREIGN KEY (`turma_id`) REFERENCES `turmas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_turma_disciplina_3` FOREIGN KEY (`voluntario_id`) REFERENCES `voluntarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `turmas`
--

DROP TABLE IF EXISTS `turmas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `turmas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) COLLATE utf8_bin NOT NULL,
  `data_criacao` datetime NOT NULL,
  `ano` varchar(4) COLLATE utf8_bin NOT NULL,
  `semestre` int(2) NOT NULL,
  `curso_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ID_UNIQUE` (`id`),
  KEY `fk_Turma_1_idx` (`curso_id`),
  CONSTRAINT `fk_turmas_1` FOREIGN KEY (`curso_id`) REFERENCES `cursos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `login` varchar(45) COLLATE utf8_bin NOT NULL,
  `senha` varchar(45) COLLATE utf8_bin NOT NULL,
  `vinculo` tinyint(2) NOT NULL,
  PRIMARY KEY (`login`),
  UNIQUE KEY `login_UNIQUE` (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `voluntarios`
--

DROP TABLE IF EXISTS `voluntarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `voluntarios` (
  `id` int(11) NOT NULL,
  `CPF` varchar(11) COLLATE utf8_bin NOT NULL,
  `RG` varchar(15) COLLATE utf8_bin NOT NULL,
  `nome` varchar(45) COLLATE utf8_bin NOT NULL,
  `sexo` varchar(1) COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `tel_residencial` varchar(18) COLLATE utf8_bin DEFAULT NULL,
  `tel_celular` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `end_logradouro` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `end_numero` varchar(6) COLLATE utf8_bin DEFAULT NULL,
  `end_complemento` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `end_bairro` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `end_cidade` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `end_UF` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `end_CEP` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `data_nascimento` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `universidade` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `numero_matricula` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `login` varchar(45) COLLATE utf8_bin NOT NULL,
  `permissao` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `CPF_UNIQUE` (`CPF`),
  UNIQUE KEY `login_UNIQUE` (`login`),
  CONSTRAINT `fk_voluntarios_1` FOREIGN KEY (`login`) REFERENCES `usuarios` (`login`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping events for database 'progestnet'
--

--
-- Dumping routines for database 'progestnet'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-11-07 12:37:08
